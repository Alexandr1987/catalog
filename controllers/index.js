const User = require('../models/user');
const passport = require('passport');

userRegistered = async (req, res, next)=>{
    const newUser = new User({
        username: req.body.username,
        email: req.body.email,
        image: req.body.image,
    });
    await User.register(newUser, req.body.password);
    res.redirect('/');
};

userLogin = async (req, res, next)=>{
    passport.authenticate('local',
        {
            successRedirect: '/',
            failureRedirect: '/users/login'
        })(req, res, next);
};

userLogOut = async (req, res, next)=>{
    req.logout();
    res.redirect('/');
};

module.exports = {
    userRegistered,
    userLogin,
    userLogOut,
};