var express = require('express');
var router = express.Router();

/* GET posts listing. */
router.get('/', (req, res, next) => {
    res.send('respond with a resource');
});

router.get('/new', (req, res, next) => {
    res.send('forn for new post');
});

router.post('/', (req, res, next) => {
    res.send('created new post');
});

router.get('/', (req, res, next) => {
    res.send('forn for new post');
});

router.get('/:id', (req, res, next) => {
    res.send('get view post');
});

router.get('/:id/edit', (req, res, next) => {
    res.send('forn for edit post');
});

router.put('/:id', (req, res, next) => {
    res.send('update post');
});

router.delete('/:id', (req, res, next) => {
    res.send('delete post');
});

module.exports = router;