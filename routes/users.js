const express = require('express');
const router = express.Router();
const passport = require('passport');
const {userRegistered, userLogin, userLogOut} = require('../controllers');
const {errorHandler} = require('../middleware');

router.get('/register', function(req, res, next) {
  res.send('get form for registration user');
});

router.post('/register', errorHandler(userRegistered));

router.get('/login', function(req, res, next) {
  res.send('form for login user');
});

router.post('/login', userLogin);

router.get('/logout', userLogOut);

router.get('/profile', function(req, res, next) {
  res.send('profile user page');
});

router.put('/profile/:id', function(req, res, next) {
  res.send('update profile user page');
});

router.get('/forgot-password', function(req, res, next) {
  res.send('page forgot password');
});

router.put('/forgot-password', function(req, res, next) {
  res.send('put forgot password');
});

module.exports = router;
