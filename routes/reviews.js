var express = require('express');
var router = express.Router({ mergeParams: true});

/* GET reviews listing. */
router.get('/', (req, res, next) => {
    res.send('respond with a reviews');
});

router.get('/new', (req, res, next) => {
    res.send('forn for new review');
});

router.post('/', (req, res, next) => {
    res.send('created new review');
});

router.get('/', (req, res, next) => {
    res.send('form for new review');
});

router.get('/:reviews_id', (req, res, next) => {
    res.send('get view review');
});

router.get('/:reviews_id/edit', (req, res, next) => {
    res.send('form for edit review');
});

router.put('/:reviews_id', (req, res, next) => {
    res.send('update review');
});

router.delete('/:reviews_id', (req, res, next) => {
    res.send('delete review');
});

module.exports = router;